package com.example.parrot.pong1;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="PONG-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;



    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------

    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------

    int score= 0;

    Point ballPosition;

    final int Ball_Width = 30;

     final int distance_from_bottom = 350;
        final int paddle_width = 100;

        final int paddle_height=20;

        Point racketPosition;

    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites

        ballPosition = new Point();
        ballPosition.x = this.screenWidth/2;

        ballPosition.y = this.screenHeight/2;


        racketPosition = new Point();
        racketPosition.x=(this.screenWidth/2) -paddle_width;
        racketPosition.y=(this.screenHeight) -distance_from_bottom-paddle_height;
        // @TODO: Any other game setup stuff goes here


    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------


    boolean movingDown =true;
            final int Ball_Speed= 100;

            boolean movingRight = true;
            final int PADDLE_SPEED= 50;
    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites

// ball moves
        if (movingDown == true) {

            ballPosition.y= ballPosition.y+Ball_Speed;

        }
      else{
          ballPosition.y = ballPosition.y-Ball_Speed;
        }

        // @TODO: Collision detection code
        if (ballPosition.y > screenHeight) {
            Log.d(TAG, "Ball reached bottom of screen. Changing direction!");
            movingDown = false;
        }

        if (ballPosition.y < 0) {
            Log.d(TAG, "Ball reached TOP of screen. Changing direction!");
            movingDown = true;
            this.score = this.score + 1;
        }

        Log.d(TAG, "Ball y-position: " + ballPosition.y);


        // ---------------------------
        // Make racket move
        // ---------------------------
        if (movingRight == true) {
            racketPosition.x = racketPosition.x + PADDLE_SPEED;
        }
        else {
            racketPosition.x = racketPosition.x - PADDLE_SPEED;
        }

        // @TODO: Collision detection code
        if (racketPosition.x > screenWidth) {
            Log.d(TAG, "Racket reached right of screen. Changing direction!");
            movingRight = false;
        }

        if (racketPosition.x < 0) {
            Log.d(TAG, "Racket reached left of screen. Changing direction!");
            movingRight = true;
        }

        Log.d(TAG, "Racket x-position: " + racketPosition.x);


    }




    // 2. Tell Android to DRAW the sprites at their positions
    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------
            // Put all your drawing code in this section

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,0,0,255));
            paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the sprites (rectangle, circle, etc)

            int left = ballPosition.x;
            int top = ballPosition.y;
            int right = ballPosition.x+Ball_Width;
            int bottom = ballPosition.y+Ball_Width;
            canvas.drawRect(left,top,right,bottom, paintbrush);

            // paddle

            int paddleleft = racketPosition.x;
            int paddletop = racketPosition.y;
            int paddleright = racketPosition.x+ 2*paddle_width;
            int paddlebottom = racketPosition.y+paddle_height;
            canvas.drawRect(paddleleft,paddletop,paddleright,paddlebottom, paintbrush);


            //@TODO: Draw game statistics (lives, score, etc)
            paintbrush.setTextSize(60);
            canvas.drawText("Score: "+ this.score, 20, 100, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    // Sets the frame rate of the game
    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // user pushed down on screen

            
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // user lifted their finger
        }
        return true;
    }
}